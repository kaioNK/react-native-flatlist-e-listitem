import React, {Component} from 'react'
import {Text, View, FlatList, ActivityIndicator} from 'react-native'
import {ListItem, SearchBar, Icon} from 'react-native-elements'

export default class FlatListDemo extends Component{
  constructor(props){
    super(props)

    this.state = {
      loading: false,
      data: [],
      page: 1, 
      seed: 1, //muda algum calculo randomico da api
      error: null,
      refreshing: false
    }
  }

  componentDidMount(){
    this.makeRemoteRequest()
    console.log(this.state.data)
  }

  makeRemoteRequest = () => {
    const {page, seed} = this.state 
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`
    this.setState({loading: true})
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        })
      })
      .catch(error => {
        this.setState({error, loading: false})
      })
  }

  renderSeparator = () => {
    return(
      <View 
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    )
  }

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round />
  }

  renderFooter = () => { //ARRUMAR ISSO PARA MOSTRAR PROX PAGINA? 
    if(!this.state.loading) return null

    return(
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 3,
          borderColor: "#CED0CE"
        }}
      >
        <Text>Ola Mundo</Text>
        <ActivityIndicator animating size="large" />
      </View>
    )
  }

  render(){
    return(
      
        <FlatList 
          keyExtractor={item => item.email}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          data={this.state.data}
          renderItem={({item}) => (
            <ListItem 
              roundAvatar
              title={`${item.name.first} ${item.name.last}`}
              subtitle={item.email}
              rightIcon={
                <Icon 
                  raised
                  name='chevron-right'
                  type='octicon'
                  color='blue'
                  size={20}
                  onPress={() => console.log(item)}
                />
              }
              leftAvatar={{
                title: 'A',
                source: {uri: item.picture.thumbnail}, 
                showEditButton: true
              }}
              style={{borderBottomWidth: 0}}
            />
          )}
        /> 
    )
  }
}

