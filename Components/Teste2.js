import React, {Component, Fragment} from 'react'
import {View, Text} from 'react-native'
import {ListItem} from 'react-native-elements'

const list = [
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  }
]

const list2 = [
  {
    title: 'Teste1',
    icon: {
      name: 'music',
      type: 'font-awesome'
    }
  },
  {
    title: 'Trips',
    icon: {
      name: 'glass',
      type: 'font-awesome'
    }
  }
]


class Teste2 extends Component{
  
  render(){
    return(
      <Fragment>
        <View>
        {
        list.map((l, i) => {
          return(
            <ListItem 
              key={i}
              leftAvatar={{source: {uri: l.avatar_url} }}
              title={l.name}
              subtitle={l.subtitle}
             />
              ) 
            })
          }
        </View>
        <View>
          {
            list2.map((item, i) => (
              <ListItem 
                key={i}
                title={item.title}
                leftIcon={item.icon}
              />
            ))
          } 
        </View>
      </Fragment>
      
    )
  }
}

export default Teste2